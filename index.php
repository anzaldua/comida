<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<title>La Casa de la Hamburguesa</title>
	<!-- Boostrap CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
	<!-- Font Awesome -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
	<!-- Font Google -->
	<link href="https://fonts.googleapis.com/css?family=Indie+Flower|Lobster" rel="stylesheet">
	<!-- main css -->
	<link rel="stylesheet" href="css/main.css">
	
</head>
<body>
	
	<header>
		
		<div class="container">
			
			<div class="row">
				
				<div class="col-md-6">
					
					<img src="img/cooper.jpg" alt="fastfood">

				</div>
				
				<div class="col-md-6">
					
					<ul class="social-header list-inline float-right mt-5">
						
						<li class="list-inline-item">
							<a href="#">
								<span class="fa-stack fa-lg">
										<i class="fab fa-facebook-f fa-stack-lx fa-inverse"></i>
								</span>
							</a>
						</li>
						<li class="list-inline-item">
							<a href="#">
								<span class="fa-stack fa-lg">
									<i class="fab fa-twitter fa-stack-lx fa-inverse"></i>
								</span>
							</a>
						</li>
						<li class="list-inline-item">
							<a href="#">
								<span class="instagram">
								  <i class="fab fa-instagram"></i>
								</span>
							</a>
						</li>
						<li class="list-inline-item">
							<a href="#">
								<span class="fa-stack fa-lg">
								  <i class="fab fa-youtube "></i>
								</span>
							</a>
						</li>

					</ul>

				</div>

			</div>

		</div>

		<div class="menu-bar">

			<nav class="container" role="menu">
				
				<ul class="menu-list list-inline">
					
					<li class="list-inline-item"><a href="#">Inicio</a></li>
					<li class="list-inline-item"><a href="#">El restaurante</a></li>
					<li class="list-inline-item"><a href="#">La carta</a></li>
					<li class="list-inline-item"><a href="#">Donde estamos</a></li>
					<li class="list-inline-item"><a href="#">Contacto y Reservas</a></li>

				</ul>

			</nav>
		
		</div>

	</header>

	<div id="main">
		<div class="container fondo-centro">
			<div id="slider-home" class="carousel slide" data-ride="carousel">
			  <ol class="carousel-indicators">
			    <li data-target="#slider-home" data-slide-to="0" class="active"></li>
			    <li data-target="#slider-home" data-slide-to="1"></li>
			    <li data-target="#slider-home" data-slide-to="2"></li>
			  </ol>
			  <div class="carousel-inner">
			    <div class="carousel-item active">
			      <img src="img/hambur.jpg" class="d-block w-100 comida" alt="...">
			      	<div class="carousel-caption">
			      		<h2>Menu de fin de semana</h2>
			      		<img src="img/promo.jpg" alt="menu 2x1" class="promo">
						<p class="h4">en todas las hamburguesas</p>
						<p>Consulta esta y todas nuestras otras promociones en nuestro apartado de carta.</p>
						<a href="#" class="btn btn-danger">Ver carta</a>
			      	</div>
			    </div>
			    <div class="carousel-item">
			      <img src="img/pizza.jpg" class="d-block w-100 comida" alt="...">
			      <div class="carousel-caption">
			      		<h2>Menu de fin de semana</h2>
			      		<img src="img/promo.jpg" alt="menu 2x1" class="promo">
						<p class="h4">en todas las pizzas</p>
						<p>Consulta esta y todas nuestras otras promociones en nuestro apartado de carta.</p>
						<a href="#" class="btn btn-danger">Ver carta</a>
			      	</div>
			    </div>
			    <div class="carousel-item">
			      <img src="img/torta.JPG" class="d-block w-100 comida" alt="...">
			      <div class="carousel-caption">
			      		<h2>Menu de fin de semana</h2>
			      		<img src="img/promo.jpg" alt="menu 2x1" class="promo">
						<p class="h4">en todas las tortas</p>
						<p>Consulta esta y todas nuestras otras promociones en nuestro apartado de carta.</p>
						<a href="#" class="btn btn-danger">Ver carta</a>
			      	</div>
			    </div>
			  </div>
			</div>

			<div class="row">
				
				<div class="col-md-6">
					
					<h3>El restaurante <strong>Cooper</strong><i class="fas fa-utensil-spoon float-right hidden-xs-down"></i></h3>

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda praesentium quia, voluptatum error id magnam magni sapiente est dolor tempora porro aperiam esse odit, ullam qui numquam? Excepturi, libero, quisquam.</p>

					<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium, officia dignissimos assumenda dicta dolores perspiciatis fugit, excepturi, quaerat voluptatem distinctio ab aliquid maiores sed itaque doloremque repudiandae fugiat nemo neque!</p>
				</div>
				<div class="col-md-6">
					
					<h3>Donde Encontrar <strong>Cooper</strong><i class="fas fa-map-marker-alt float-right hidden-xs-down"></i></i></h3>

					<p>Nos encontramos cerca de tu corazon</p>

					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3595.4850715137745!2d-100.24415218459099!3d25.688358917900928!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8662eaa399ad3381%3A0x4ac62fe481e0eade!2sUNEME+DEDICAM!5e0!3m2!1ses-419!2smx!4v1556352303772!5m2!1ses-419!2smx" width="100%" height="205" frameborder="0" style="border:0" allowfullscreen></iframe>

				</div>

			</div>

			<div class="row camara">
				<div class="col-md-12">
					<h3>El restaurante <strong>Cooper</strong> por dentro <i class="fas fa-camera float-right hidden-xs-down"></i></h3>
				</div>
			</div>	
				
			<div class="row lugar">
				
				<div class="col-sm-4">
					<img src="img/lugar.jpg" alt="Imagen 1" class="img-fluid">
				</div>
				<div class="col-sm-4">
					<img src="img/lugar5.jpg" alt="Imagen 2" class="img-fluid">
				</div>
				<div class="col-sm-4">
					<img src="img/lugar4.jpg" alt="Imagen 3" class="img-fluid">
				</div>

			</div>

			<div class="row sidebar-zone">
				
				<div class="col-sm-4">
					
					<h4>Visita <strong>Cooper</strong></h4>
					
					<p>Estamos abiertos todos los dias de la semana el domingo cerramos a las 8pm de la noche, el resto de la semana a las 10pm</p>	
					
					<p>Nos ubicamos en la calle milan entre paris y notrensburgo</p>

					<p>Para reservas puedes consultar nuestro <strong>apartado de contacto</strong>o llamarnos al xxx xxx xxx</p>
				</div>

				<div class="col-sm-4">
					
					<h4>Ofertas Recomendadas</h4>

					<ul class="list-unstyled">
						
						<li><a href="#">Menu de fin de semana 2x1 en hamburguesas</a></li>
						<li><a href="#">Cena para parejas los martes</a></li>
						<li><a href="#">Menu "Come hasta explotar"</a></li>
						<li><a href="#">La mega Hamburguesa</a></li>

					</ul>

				</div>
				
				<div class="col-sm-4">
					
					<h4>Siguenos en nuestras redes sociales</h4>

					<ul class="list-unstyled">
						
						<li><a href="#">Facebook</a></li>
						<li><a href="#">Twitter</a></li>
						<li><a href="#">Instagram</a></li>
						<li><a href="#">Youtube</a></li>

					</ul>

				</div>

			</div>
		</div>
	</div>

	
	<footer>
		
		<div class="container">
			
			<div class="row footer">

				<div class="col-sm-6">
				
					<strong>Cooper</strong> &copy:2019 - Todos los derechos reservados. Sitio Hecho por <a href="#">Alejandro Anzaldua</a>

				</div>

				<div class="col-sm-6">
					
					<ul class="list-inline float-right">
						
						<li class="list-inline-item">
							<a href="#">Inicio</a>
						</li>
						<li class="list-inline-item">
							<a href="#">Aviso Legal</a>
						</li>
						<li class="list-inline-item">
							<a href="#">Cookies</a>
						</li>
						<li class="list-inline-item">
							<a href="#">Contacto</a>
						</li>

					</ul>

				</div>

			</div>

		</div>


	</footer>

	<!-- Boostrap JS -->
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
	<!-- Js Main Script -->
	<script src="js/main.js"></script>
</body>
</html>